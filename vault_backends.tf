resource "vault_mount" "secrets" {
  for_each  = vault_namespace.namespaces
  namespace = each.value.path_fq
  path      = "kv-v2"
  type      = "kv"
  options = {
    version = "2"
  }
}

resource "vault_auth_backend" "jwt" {
  type = "jwt"
}

resource "vault_jwt_auth_backend" "jwt" {
    for_each  = vault_namespace.namespaces
    namespace = each.value.path_fq
    path                = "jwt"
    jwks_url            = "https://gitlab.com/-/jwks"
    bound_issuer        = "gitlab.com"
}