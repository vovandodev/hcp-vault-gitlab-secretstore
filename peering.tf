# // This resource initially returns in a Pending state, because its application_id is required to complete acceptance of the connection.
# resource "hcp_azure_peering_connection" "peer" {
#   hvn_link                 = hcp_hvn.default.self_link
#   peering_id               = "azure-hvn"
#   peer_vnet_name           = data.azurerm_virtual_network.vnet.name
#   peer_subscription_id     = data.azurerm_subscription.sub.subscription_id
#   peer_tenant_id           = var.tenant_id
#   peer_resource_group_name = data.azurerm_resource_group.rg.name
#   peer_vnet_region         = data.azurerm_virtual_network.vnet.location
# }

# // This data source is the same as the resource above, but waits for the connection to be Active before returning.
# data "hcp_azure_peering_connection" "peer" {
#   hvn_link              = hcp_hvn.default.self_link
#   peering_id            = hcp_azure_peering_connection.peer.peering_id
#   wait_for_active_state = true
# }

# data "azurerm_resource_group" "rg" {
#   name = var.resource_group_name
# }

# data "azurerm_virtual_network" "vnet" {
#   name = var.virtual_network_name
#   resource_group_name = data.azurerm_resource_group.rg.name
# }

# // The route depends on the data source, rather than the resource, to ensure the peering is in an Active state.
# resource "hcp_hvn_route" "route" {
#   hvn_link         = hcp_hvn.default.self_link
#   hvn_route_id     = "azure-route"
#   destination_cidr = var.destination_cidr
#   target_link      = data.hcp_azure_peering_connection.peer.self_link
# }

# resource "azuread_service_principal" "principal" {
#   client_id = hcp_azure_peering_connection.peer.application_id
# }

# resource "azurerm_role_definition" "definition" {
#   name  = join("-", ["hcp-hvn-peering-access", hcp_azure_peering_connection.peer.application_id])
#   scope = data.azurerm_virtual_network.vnet.id

#   assignable_scopes = [
#     data.azurerm_virtual_network.vnet.id
#   ]

#   permissions {
#     actions = [
#       "Microsoft.Network/virtualNetworks/peer/action",
#       "Microsoft.Network/virtualNetworks/virtualNetworkPeerings/write",
#       "Microsoft.Network/virtualNetworks/virtualNetworkPeerings/read"
#     ]
#   }
# }

# resource "azurerm_role_assignment" "assignment" {
#   principal_id       = azuread_service_principal.principal.id
#   scope              = data.azurerm_virtual_network.vnet.id
#   role_definition_id = azurerm_role_definition.definition.role_definition_resource_id
# }