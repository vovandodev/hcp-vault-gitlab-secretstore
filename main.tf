resource "hcp_project" "default" {
  name        = var.hcp_project_id
}

resource "hcp_hvn" "default" {
  hvn_id         = var.hvn_id
  cloud_provider = var.cloud_provider
  region         = var.region
  cidr_block     = var.hvn_cidr_block
}

resource "hcp_vault_cluster" "default" {
  hvn_id          = hcp_hvn.default.hvn_id
  cluster_id      = var.cluster_id
  public_endpoint = var.public_endpoint
  tier            = var.tier
  # lifecycle {
  #   prevent_destroy = true
  # }
}

resource "hcp_vault_cluster_admin_token" "default" {
  cluster_id = hcp_vault_cluster.default.cluster_id
}