resource "vault_jwt_auth_backend_role" "vovandodev" {
  backend         = vault_jwt_auth_backend.jwt["vovandodev"].path
  namespace       = vault_namespace.namespaces["vovandodev"].path_fq
  role_name       = "vovandodev-root"
  token_policies  = ["vovandodev-root"]
  bound_claims_type = "glob"
  bound_claims = {
    "namespace_path": "vovandodev/*"
  }
  user_claim      = "user_login"
  role_type       = "jwt"
  token_max_ttl   = 60
}