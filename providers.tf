terraform {
  required_version = "~> 1.0"
  required_providers {
    hcp = {
      source  = "hashicorp/hcp"
      version = "0.77.0"
    }
    azuread = { source = "hashicorp/azuread", version = "~> 2.0" }
    azurerm = { source = "hashicorp/azurerm", version = "~> 3.0" }
  }
}

provider "hcp" {
  
}

##For peering shouldn't be commented
# data "azurerm_subscription" "sub" {
#   subscription_id = var.subscription_id
# }

# provider "azurerm" {
#   features {}
# }

# provider "azuread" {}