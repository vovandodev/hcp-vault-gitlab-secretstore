resource "vault_policy" "vovandodev" {
  name = "vovandodev-root"
  namespace = vault_namespace.namespaces["vovandodev"].path_fq

  policy = <<EOT
path "kv-v2/data/security/*" {
  capabilities = ["read"]
}
EOT
}
