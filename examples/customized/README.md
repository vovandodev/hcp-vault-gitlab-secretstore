# Customized

These variables are overwritten.

```hcl
hvn_id          = "custom"
cluster_id      = "custom"
region          = "eastus"
public_endpoint = true
tier            = "dev"
```
