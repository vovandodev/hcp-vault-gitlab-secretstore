module "hcp_cluster" {
  source          = "../../"
  region          = "eastus"
  public_endpoint = true
  tier            = "dev"
}
