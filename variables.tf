// HCP variables
variable "hcp_project_id" {
  default = "Hashi-Vault-Project" 
}

variable "hvn_id" {
  description = "The ID of the HCP HVN."
  type        = string
  default     = "hcp-vault-hvn"
}

variable "cluster_id" {
  description = "The ID of the HCP Vault cluster."
  type        = string
  default     = "hcp-vault"
}

variable "region" {
  description = "The region of the HCP HVN and Vault cluster."
  type        = string
  default     = "eastus"
}

variable "cloud_provider" {
  description = "The cloud provider of the HCP HVN and Vault cluster."
  type        = string
  default     = "azure"
  validation {
    condition     = var.cloud_provider == "azure"
    error_message = "The cloud_provider can only be 'azure'."
  }
}

variable "public_endpoint" {
  description = "Denotes that the cluster has a public endpoint."
  type        = bool
  default     = true
}

variable "tier" {
  description = "Tier of the HCP Vault cluster."
  type        = string
  default     = "dev"
  validation {
    condition     = contains(["dev", "standard_small", "standard_medium", "standard_large"], var.tier)
    error_message = "The tier can be: 'dev', 'standard_small', 'standard_medium' or 'standard_large'."
  }
}

variable "hvn_cidr_block" {
  default = "172.25.16.0/20"
  
}

variable "virtual_network_name" {
  default = "hvn"
}

variable "destination_cidr" {
  default = "10.0.0.0/16"
}

// Vault variables
variable "namespaces" {
  type = set(string)
  default = [
    "vovandodev",
    "Foundation",
    "lab",
    "etc."
  ]
}

//Variables for the peering
# variable "tenant_id" {
# }

# variable "subscription_id" {
# }

# variable "resource_group_name" {

# }