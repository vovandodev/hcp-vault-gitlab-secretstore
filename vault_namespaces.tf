provider "vault" {
  address = hcp_vault_cluster.default.vault_public_endpoint_url
  token   = hcp_vault_cluster_admin_token.default.token
}

resource "vault_namespace" "namespaces" {
  for_each  = var.namespaces
  path      = each.key
}


